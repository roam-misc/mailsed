#ifndef __INCLUDED_mailsed_h
#define __INCLUDED_mailsed_h

/*-
 * Copyright (c) 2001, 2002, 2010  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifdef __GNUC__
#define __UNUS	__attribute__((unused))
#else
#define __UNUS
#endif /* __GNUC__ */

#ifdef EBUG
#define RDBG(x)         if (quiet < 2) { printf x; }
#else /* !EBUG */
#define RDBG(x)
#endif /* EBUG */

static char mailsed_h_cvs_id[] __UNUS = "$Id: mailsed.h,v 1.4 2000/12/16 16:04:00 roam Exp $";

/* * *		Definitions		* * */

typedef enum {
  MS_ERR_NONE,
  MS_ERR_NOMEM, MS_ERR_CMDLINE,
  MS_ERR_SIGOPEN, MS_ERR_SIGREAD,
  MS_ERR_PIPE, MS_ERR_FORK, MS_ERR_WRITE,
  MS_ERR_INT,
  MS_ERR_LAST
} ms_err_t;

#define MS_OPTSTR	"hm:Oqs:vV"

#define MS_SENDMAIL	"/var/qmail/bin/qmail-inject"
#define MS_SIGFILE	"/home/roam/.sig/default"

typedef struct {
  char	*s;
  int	len;
} ms_sig_t;

#define MS_SIGALLOC	8		/* allocate this many at a time */

/* * *		Variables		* * */

extern int	verbose, quiet;
extern char	ms_verstr[];

/* * *		Functions		* * */

ms_err_t	prerror(const char *s, ms_err_t code);

ms_err_t	ms_init(int argc, char **argv);
ms_err_t	ms_close(void);
ms_err_t	ms_help(void);
ms_err_t	ms_version(void);

ms_err_t	ms_readloop(int fd);

#endif /* __INCLUDED_mailsed_h */
