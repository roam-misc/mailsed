# Copyright (c) 2001, 2002, 2010  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# $Id: Makefile,v 1.3 2000/06/19 18:31:43 roam Exp $

CC		= gcc
CFLAGS_WARN	= -Wall -W -Wstrict-prototypes -Wmissing-prototypes -Wwrite-strings
CFLAGS_INC	= -I. -I..
CFLAGS_OPT	=
CFLAGS_OPT	= -O6
#CFLAGS_DBG	= -ggdb -g3 -DEBUG
CFLAGS_DBG	=
CFLAGS_MISC	=

CFLAGS		?= ${CFLAGS_WARN} ${CFLAGS_OPT} ${CFLAGS_DBG}
CFLAGS		+= ${CFLAGS_INC} ${CFLAGS_MISC}

LDFLAGS_WARN	= -Wall -W
#LDFLAGS_OPT	=
LDFLAGS_OPT	= -O6
#LDFLAGS_DBG	= -ggdb -g3
LDFLAGS_DBG	=
LDFLAGS_MISC	=

LDFLAGS		?= ${LDFLAGS_WARN} ${LDFLAGS_OPT} ${LDFLAGS_DBG}
LDFLAGS		+= ${LDFLAGS_MISC}

INSTALL		= /usr/bin/install -c

#INSTOWN		= -o bin
INSTOWN		=
#INSTGRP		= -g bin
INSTGRP		=
INSTMODE	= -m 755

PREFIX		= /home/roam
BINDIR		= ${PREFIX}/bin
LIBDIR		= ${PREFIX}/lib
SHAREDIR	= ${PREFIX}/share

TGT		= mailsed
OBJS		= mailsed.o
SRCC		= mailsed.c
SRCH		= mailsed.h
LIBS		=

MS_VER_MAJ	=0
MS_VER_MIN	=01

FLAGS_VER	= -DMS_VER_MAJ=${MS_VER_MAJ} -DMS_VER_MIN=${MS_VER_MIN} -DMS_OS=\"`uname -s`\" -DMS_OSREL=\"`uname -r`\" -DMS_OSHOST=\"`uname -n`\"

DISTDIR		= /home/roam/lang/c/dist
DISTFILE_	= ${TGT}-${MS_VER_MAJ}.${MS_VER_MIN}

DISTFILE	= ${DISTFILE_}.tar.gz


DISTC		= ${SRCC}
DISTH		= ${SRCH}
DISTDOC		=
DISTADD		= Makefile makedep.sh .depend .dist-exclude
DISTNAMES	= ${DISTC} ${DISTH} ${DISTDOC} ${DISTADD}

all:		${TGT}

clean:
		rm -f ${TGT} ${OBJS}

distclean:	clean
		touch .depend

depend:
		sh makedep.sh ${SRCC}

dist:		distclean
		@echo making dist to ${DISTDIR}/${DISTFILE}
		tar -cvf ${DISTDIR}/${DISTFILE} --exclude=CVS -C .. ${TGT}

install:	install-bin install-lib install-share

install-bin:	${TGT}
		-mkdir -p ${BINDIR}
		${INSTALL} ${INSTOWN} ${INSTGRP} ${INSTMODE} ${TGT} ${BINDIR}/${TGT}

install-lib:

install-share:

${TGT}:		${OBJS}
		${CC} ${LDFLAGS} -o ${TGT} ${OBJS} ${LIBS}

.c.o:
		${CC} -c ${CFLAGS} ${FLAGS_VER} $<

include .depend
