/*-
 * Copyright (c) 2001, 2002, 2010  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "mailsed.h"

static char cvs_id[] __UNUS = "$Id: mailsed.c,v 1.5 2000/12/16 16:04:00 roam Exp $";

int	verbose = 0, quiet = 0;
char	ms_verstr[64] = "mailsed";

static const char *errs[MS_ERR_LAST] = {
  "no error",
  "out of memory",
  "bad command-line arguments",
  "opening signature file",
  "reading signature file",
  "pipe() error",
  "fork() error",
  "write() error",
  "INTERNAL ERROR"
};

static int errsn[MS_ERR_LAST] = {
  0, 0, 0,	/* NONE, NOMEM, CMDLINE */
  1, 1,		/* SIGOPEN, SIGREAD */
  1, 1, 1,	/* PIPE, FORK, WRITE */
  0		/* INT */
};

static char	smpath[NAME_MAX+1] = MS_SENDMAIL;
static int	sendfd = -1;
static char	sigfname[NAME_MAX+1] = MS_SIGFILE;
static char	*sigbuf = NULL;
static ms_sig_t	*sigs = NULL;
static int	sigcnt = 0, signextalloc = 0;
static int	ms_stdout = 0;

/***
Function:
	prerror				- print error message and context
Inputs:
	s				- context string to be printed
	code				- error code
Returns:
	code
Modifies:
	nothing; writes to stderr
***/

ms_err_t
prerror(const char *s, ms_err_t code) {
  char buf[256];

  if (code > MS_ERR_INT)
    code = MS_ERR_INT;

  snprintf(buf, sizeof(buf), "mailsed error: %s: %s", s, errs[code]);

  if (errsn[code])
    perror(buf);
  else
    fprintf(stderr, "%s\n", buf);

  return code;
}

/***
Function:
        ms_makeversion()                - generate version string
Inputs:
        none
Returns:
        ms_err_t
Modifies:
        ms_verstr
***/

static ms_err_t
ms_makeversion(void) {

  snprintf(ms_verstr, sizeof(ms_verstr), "mailsed v%d.%02d", MS_VER_MAJ, MS_VER_MIN);
  return MS_ERR_NONE;
}

/***
Function:
	ms_storesig()				- parse single sigline
Inputs:
	s					- sigline
	len					- well.. len of sigline :)
Returns:
	ms_err_t
	MS_ERR_NOMEM
Modifies:
	sigs, sigcnt, possibly signextalloc
***/

static ms_err_t
ms_storesig(char *s, int len) {
  ms_sig_t *p;

  if (len == 0) {
    RDBG(("ms_storesig: cowardly refusing to store an empty sig\n"));
    return MS_ERR_NONE;
  }

  if (sigcnt >= signextalloc) {
    signextalloc += MS_SIGALLOC;
    if (p = realloc(sigs, sizeof(*sigs) * signextalloc), p == NULL)
      return MS_ERR_NOMEM;
    sigs = p;
  }

  sigs[sigcnt].s = s;
  sigs[sigcnt].len = len;
  sigcnt++;
  RDBG(("ms_storesig, sig %d, size %d\n", sigcnt, len));
  return MS_ERR_NONE;
}

/***
Function:
	ms_readsigfile()			- read in a list of sigs
Inputs:
	fname					- sigfile name
Returns:
	ms_err_t
Modifies:
	sigcnt, sigs, sigbuf
***/

static ms_err_t
ms_readsigfile(const char *fname) {
  FILE *fp;
  size_t sigsize;
  char *b, *p;
  int len;

  if (fp = fopen(fname, "rt"), fp == NULL)
    return MS_ERR_SIGOPEN;
  fseek(fp, 0, SEEK_END);
  sigsize = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  if (sigbuf) { free(sigbuf); sigbuf = NULL; }
  if (sigs) { free(sigs); sigs = NULL; }
  sigcnt = 0;

  if (sigbuf = malloc(sigsize), sigbuf == NULL)
    return MS_ERR_NOMEM;
  if (fread(sigbuf, sigsize, 1, fp) != 1)
    return MS_ERR_SIGREAD;
  fclose(fp);

  sigcnt = 0; signextalloc = 0;
  b = sigbuf; len = sigsize;
  while(p = memchr(b, '\n', len), p) {
    ms_storesig(b, p-b);
    p++;
    len -= p - b; b = p;
  }
  ms_storesig(b, len);

  return MS_ERR_NONE;
}

/***
Function:
	ms_init				- general-purpose init function
Inputs:
	argc, argv			- main() args for option processing
Returns:
	ms_err_t
	CMDLINE
	ms_readsigfile() errors
Modifies:
	quiet, verbose, smpath, sigfname
	calls ms_readsigfile()
***/

extern int optind, opterr;

ms_err_t
ms_init(int argc __UNUS, char **argv __UNUS) {
  int ch, helpq = 0, versq = 0;
  ms_err_t r;

  ms_makeversion();

  /* Process cmdline options */
  
  opterr = 0;
  while(ch = getopt(argc, argv, MS_OPTSTR), ch != EOF)
    switch(ch) {
      case 'h':
	helpq = 1;
	break;
      case 'm':
	strncpy(smpath, optarg, sizeof(smpath));
	smpath[sizeof(smpath)-1] = 0;
	break;
      case 'O':
	ms_stdout = 1;
	break;
      case 'q':
	quiet++;
	break;
      case 's':
	strncpy(sigfname, optarg, sizeof(sigfname));
	sigfname[sizeof(sigfname)-1] = 0;
	break;
      case 'V':
	versq = 1;
	break;
      case 'v':
	verbose++;
	break;
      case '?':
      default:
	return MS_ERR_CMDLINE;
    }

  argc -= optind; argv += optind;

  if (helpq) {
    ms_version();
    ms_help();
    exit(MS_ERR_NONE);
  } else if (versq) {
    ms_version();
    exit(MS_ERR_NONE);
  }

  srandom(time(NULL));
  if (r = ms_readsigfile(sigfname), r)
    return r;
  
  return MS_ERR_NONE;
}

/***
Function:
	ms_close			- general-purpose shutdown function
Inputs:
	none
Returns:
	ms_err_t
Modifies:
	closes sendfd if opened
***/

ms_err_t
ms_close(void) {

  if (sendfd > 1)
    close(sendfd);
  
  return MS_ERR_NONE;
}

/***
Function:
	ms_help					- startup help info
Inputs:
	none
Returns:
	MS_ERR_NONE
Modifies:
	nothing, writes to stdout
***/

ms_err_t
ms_help(void) {
  
  puts("Usage: mailsed [-hqvV]\n"
       "\t-h\tprint this help text and exit;\n"
       "\t-v\tverbose operation; multiple -v's increase verbosity level;\n"
       "\t-q\tquiet operation; multiple -q's make it even more quiet;\n"
       "\t-V\tprint version information and exit."
       );
  return MS_ERR_NONE;
}

/***
function:
	ms_version				- output version info
Inputs:
	none
Returns:
	MS_ERR_NONE
Modifies:
	nothing, writes to stdout
***/

ms_err_t
ms_version(void) {

  puts(ms_verstr);

  if (verbose) {
    puts("Built on " __DATE__ ", " __TIME__);
#ifdef __GNUC__
    if (verbose > 1)
      puts("Compiler: GNU C " __VERSION__ " on " MS_OS " " MS_OSREL
	   ": " MS_OSHOST);
#endif /* __GNUC__ */
  }
  return MS_ERR_NONE;
}

/***
Function:
	ms_sigchld()				- SIGCHLD handler
Inputs:
	none
Returns:
	nothing
Modifies:
	nothing (FIXME: or should it?)
***/

static void
ms_sigchld(int sig) {
  
  wait(&sig);
}

/***
Function:
	ms_execsend()				- start sendmail
Inputs:
	none
Returns:
	ms_err_t
Modifies:
	nothing so far, tries to exec sendmail in a subprocess
***/

static ms_err_t
ms_execsend(const char *smpath) {
  int pid, fds[2];

  if (ms_stdout) {
    sendfd = 1;
    return MS_ERR_NONE;
  }

  if (pipe(fds))
    return MS_ERR_PIPE;
 
  signal(SIGCHLD, ms_sigchld);
  
  if (pid = fork(), pid < 0)
    return MS_ERR_FORK;
  
  if (pid == 0) {
    /* child */
    close(fds[1]);
    close(0); close(1); close(2);
    dup2(fds[0], 0);
    execl(smpath, "sendstuff-sendmail", NULL);
    abort();
  }

  /* parent */
  close(fds[0]);
  sendfd = fds[1];

  return MS_ERR_NONE;
}

/***
Function:
	do_write()				- write() in a loop
Inputs:
	fd					- fdesc to write to
	buf					- data to write
	count					- size of the data to write
Returns:
	ms_err_t
Modifies:
	nothing; writes to fd
***/

static ms_err_t
do_write(int fd, const void *buf, size_t count)
{
  ssize_t n;

  while (count > 0) {
    n = write(fd, buf, count);
    if (n < 0)
      return MS_ERR_WRITE;
    buf += n;
    count -= n;
  }
  return MS_ERR_NONE;
}

/***
Function:
	ms_writesig()				- output a signature
Inputs:
	fd					- fdesc to write to
Returns:
	ms_err_t
Modifies:
	nothing; writes to fd
***/

static ms_err_t
ms_writesig(int fd) {
  int n;

  n = random() % sigcnt;
  return do_write(fd, sigs[n].s, sigs[n].len);
}

/***
Function:
	ms_writesubstr()			- subst sig into new file
Inputs:
	fd					- fd to write to
	str					- string to subst into
	len					- length of string
Returns:
	ms_err_t
Modifies:
	nothing; writes to fd
***/

static ms_err_t
ms_writesubst(int fd, char *str, int len) {
  char *p = str;
  ms_err_t r;

  while(p = memchr(str, '~', len), p) {
    if (p > str)
      if (r = do_write(fd, str, p-str), r != MS_ERR_NONE)
	return r;
    
    if ((p < str + len - 5) && !memcmp(p, "~SIG~", 5)) {
      ms_writesig(fd);
      p += 5;
    } else {
      if (r = do_write(fd, p, 1), r != MS_ERR_NONE)
	return r;
      p++;
    }
    len -= p - str; str = p;
  }

  return do_write(fd, str, len);
}

/***
Function:
	ms_readloop()				- read/send loop
Inputs:
	fd					- filedesc to write to
Returns:
	ms_err_t
Modifies:
	nothing; writes/reads
***/

ms_err_t
ms_readloop(int fd) {
  char buf[512], line[513], *c, *nxt;
  int n;
  unsigned ln = 0, lb;
  ms_err_t r;

  while(n = read(0, buf, sizeof(buf)), n > 0) {
    c = buf; lb = n;
    while(nxt = memchr(c, '\n', lb), nxt) {
      nxt++; /* found the \n, eat it */
      if (ln + (nxt - c) >= sizeof(line)) {
	if (r = ms_writesubst(fd, line, ln), r != MS_ERR_NONE)
	  return r;
	ln = 0;
      }
      /* alright, line+ln has enough space to hold the new line */
      memcpy(line+ln, c, nxt-c);
      if (r = ms_writesubst(fd, line, ln + nxt - c), r != MS_ERR_NONE)
	return r;
      ln = 0;

      lb -= nxt - c; c = nxt;
    }
    
    /* not ending on a newline */
    if (ln + lb >= sizeof(line)) {
      if (r = ms_writesubst(fd, line, ln), r != MS_ERR_NONE)
	return r;
      ln = 0;
    }
    memcpy(line+ln, c, lb);
    ln += lb;
  }

  if (ln)
    if (r = ms_writesubst(fd, line, ln), r != MS_ERR_NONE)
      return r;

  return MS_ERR_NONE;
}

/***
  M A I N   F U N C T I O N
***/

int main(int argc __UNUS, char **argv __UNUS) {
  ms_err_t r;

  if (r = ms_init(argc, argv), r)
    return prerror("init", r);

  if (r = ms_execsend(smpath), r)
    return prerror("execsend", r);

  if (r = ms_readloop(sendfd), r)
    return prerror("readloop", r);

  if (r = ms_close(), r)
    return prerror("close", r);

  return 0;
}
